import React from 'react';
import Logo from './images/logo.svg';



const Privacy=()=>{
  return(
      <>
        <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light">
   <div className="container">
  <a className="navbar-brand" href="#"><img src={Logo}/></a>
   <i className="fa fa-map-marker-alt fa-map-marker map-marker"></i>

  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item dropdown">
        <a className="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pune
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Action</a>
          <a className="dropdown-item" href="#">Another action</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Become a New Customer</a>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <button className="btn my-2 my-sm-0" type="submit">Order Delivery</button>
      <button className="btn my-2 my-sm-0" type="submit"><a href="signup.js"></a>Login/Signup</button>
    </form>
  </div>
  </div>
</nav>

    <br></br><br></br><br></br>
    {/* Privacy section */}

    <h1>Customer issues</h1>
    
    <div className="container">
    <span>Do I need to place a minimum number of orders per month?
No. There is no minimum. You can just make one order a year!
How can I book a delivery?
There are three ways: using the online form on our website, by phone or API.
How early should I book a courier?
You only need to book 30 minutes in advance if the first delivery is within travel zone 1, one hour in advance if in travel zone 2 and 1.5–2 hours if further out of .
How do I know that the courier is from Wefast and not from somewhere else?
The courier will ask you to sign in the app on his/her smartphone. In addition, the courier will contact you before arriving at the address so you will be able to identify him/her.
How we work
How do you work with online stores?
We have tried to answer all your questions about delivering for online stores here.
Do you pack the goods?
No, the courier will deliver the goods in the same packaging they were collected in.
Booking and changing an order
Can I choose a specific courier for my delivery?
When you fill in the form, you can use the free Preferred Courier service in your online account. You can choose couriers you have used before and whom you have given a 5 star rating. Your chosen courier will receive a message that you would like them to deliver your order. If the courier wants to take the order, he/she clicks on it.
I have a very urgent order. Can I specify a 30 minute interval between two deliveries?
Yes, as long as this time is sufficient for the courier to get from one address to the other.
What do I do if I want to send several items to the recipient to choose from?
On the form, add your collection point separately as a return address.
Can I order delivery by car?
Yes. We will mark the order for a car if the item weighs more than 15 kg. However, you will be charged a different delivery price, see rates.
Can I order a specific car?
We don't know in advance what car the courier is driving. To make sure you get a courier whose car is big enough, specify the dimensions and weight of the item. You will get responses from couriers who have cars big enough to fulfil your order.
Can I cancel or change my order?
If you change the order after the courier has set off, you will be charged ₹ 200. You can make changes through your online account or by phone, but these changes have to be agreed with the courier.
Fulfilling orders
Will you definitely find a courier to deliver my order?
We can always find a courier, the only issue is time. For example, it can sometimes take up to an hour to find a courier if you book a courier on Friday, which is the busiest day of the week.
Why hasn't anyone been assigned to my order yet?
Our system works on the basis of responses. So far, no couriers have responded to your order. Depending on the day and the route, it can take from one minute to an hour to find a courier.
How do I know if the courier has delivered the parcel?
You will receive a text message or email. The recipient will also receive a text message notifying them that the courier is coming (unless the delivery is a surprise).</span>
</div>

<hr></hr>
   {/* footer section */}
   <div className="f-data">
     <div className="container">
       <div className="row">
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <img src={Logo} />
           <ul>
             <li><a href="#">Mumbai</a></li>
             <li><a href="#">Pune</a></li>
             <li><a href="#">Kolkata</a></li>
             <li><a href="#">Delhi/NCR</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* second column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">Subscription Plans</a></li>
             <li><a href="/FaQ">FAQ</a></li>
             <li><a href="#">Disclaimers & Dispute Resolution</a></li>
             <li><a href="#">Refund and Cancellation</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* third column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">About Us</a></li>
             <li><a href="/PRivacy">Privacy and Policies</a></li>
             <li><a href="#">Contact</a></li>
             <li><a href="#">Track Order</a></li>
             <li><a href="#">Courier Service in Pune</a></li>
             <li><a href="#">pricing</a></li>
             <li><a href="/TermCondition">Terms and Conditions</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* fourth column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
           <div className="play-store">
             <li><a href="#"><div className="app-store"></div></a></li>
             <li><a href="#"><div className="play-store"></div></a></li>
             <li><a href="#"><div className="app-gallery"></div></a></li>
             </div>
           </ul>
         </div>     {/*col-lg-3*/}
       </div>
     </div>
   </div>    {/*f-data */}
{/* social-media section */}
 <div className="social-section">
  <div className="s-icon">
    <i className="fa fa-facebook-square icon"></i>
    <i className="fa fa-youtube-square icon"></i>
     <i className="fa fa-instagram icon"></i>
    <i className="fa fa-youtube-square icon"></i>
  </div>
   <p>© 2017–2021 Wefast. All Rights reserved.</p> 
 </div>      {/* social section */} 
      </>


  )
};

export default Privacy;