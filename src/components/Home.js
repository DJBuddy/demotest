import React from 'react';
import Logo from './images/logo.svg';
import Dropdown from 'react-bootstrap/Dropdown';
// import Button from 'react-bootstrap/Button';
import { Navbar, Button, NavbaTtoggler, NavbarTogglerIcon  } from 'react-bootstrap';

const Home =() =>{
 return(
     <>
   <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light">
   <div className="container">
  <a className="navbar-brand" href="#"><img src={Logo}/></a>
   <i className="fa fa-map-marker-alt fa-map-marker map-marker"></i>

  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item dropdown">
        <a className="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pune
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Action</a>
          <a className="dropdown-item" href="#">Another action</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Become a New Customer</a>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <button className="btn my-2 my-sm-0" type="submit">Order Delivery</button>
      <button className="btn my-2 my-sm-0" type="submit"><a href="signup.js"></a>Login/Signup</button>
    </form>
  </div>
  </div>
</nav>

{/* HEADER PROMO */}
<div className="promo-box">
<div className="promo-img"> 
<div className="container">
<div className="row">
<div className="col-lg-6 col-md-6 col-sm-12 col-12">
  <p>Fastest courier service in pune<br></br><span>Low-priced same day delivery service!</span></p>
</div>
{/* form section */}

<div className="col-lg-6 col-md-6 col-sm-12 col-12">
<form>
  <div className="form-group">
  <i className="fa fa-map-marker-alt fa-map-marker address d-flex"></i>
    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Pickup Address" />
  </div>
  <div className="form-group">
   <i className="fa fa-map-marker-alt fa-map-marker address d-flex"></i>
    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Delivery Address" />
  </div>
  <button type="submit" className="btn btn-success "><span>Book a courier</span></button>
   <h6>Book without registering, urgent delivery at no extra cost</h6>

 </form> 
 </div>   {/*col-lg-6*/}
 </div>   {/*row*/}
 </div>  {/* container*/}
</div>
</div>      {/*box-img*/}


{/* CARD SECTION */}

<div className="card-section">
   <div className="card-item">
     <div className="sub-card">
       <div className="content">
        <i className="fa fa-hourglass-half icon"></i>
        <span>90 mins</span>
       </div>
       <p>We can deliver ASAP or at a specified time documents, products, flowers, any product </p> 
     </div>
     {/* 2nd card */}
     <div className="sub-card">
       <div className="content">
        <i className="fa fa-hourglass-half icon"></i>
        <span>90 mins</span>
       </div>
       <p>Start from ₹ 40 for hyperlocal tariff in Pune and ₹ 8 per km in zone 1 </p> 
     </div>
     {/* 3rd card */}
     <div className="sub-card">
       <div className="content">
        <i className="fa fa-cogs icon"></i>
        <span>90 mins</span>
       </div>
       <p>Selling online? Place orders via API Find the guidelines at <a href="#"><br />API Docs</a> </p>

     </div>
   </div>
</div>        {/*card-section*/}


{/* DOWNLOAD SECTION */}

<div className="download">
<div className="container">
  <div className="row">
      <div className="col-lg-6 col-md-6 col-sm-12 col-12">
          <h2>Download the app and all couriers will be at your hand</h2><br></br>
          <p>In any free minute, create new orders and monitor the implementation of already created.</p>

          <div className="row">
            <div className="col-lg-6 col-md-6 col-sm-12 col-12">
            <div className="box">
              <div className="scan-img"></div>
              </div>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-12 col-12">
              <div className="store">
                <ul>
                  <li><div className="store-one"></div></li>
                  <li><div className="store-two"></div></li> 
                  <li><div className="store-three"></div></li>
                </ul>
              </div>
            </div>
          </div>
       </div>     {/*col-lg-6*/}

      {/* moblie image */}
      <div className="col-lg-6 col-md-6 col-sm-12 col-12">
        <div className="moblie">
        {/* <img src={Mobile} /> */}
        </div>
      </div>
    </div>           {/*row*/}
  </div>              {/* contianer */}
</div>               {/*download*/}


{/* Layout section */}
<div className="layout-section">
   <h2>Why choose Us?</h2>
   <div className="container">
      <div className="row">
        <div className="col-lg-4 col-md-4 col-sm-12 col-12">
          <div className="text d-flex">
          <i className="fa fa-rocket icon"></i>
            <p>Walking couriers and riders are always available. We assign the nearest courier with the highest rating within 7 minutes.</p>
          </div>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-12 col-12">
          <div className="text d-flex">
          <i className="fa fa-rocket icon"></i>
            <p>The system assigns high-scored and closest courier. Probably we provide you with fastest delivery service ever.</p>
          </div>
        </div>
        <div className="col-lg-4 col-md-4 col-sm-12 col-12">
          <div className="text d-flex">
          <i className="fa fa-star icon"></i>
            <p>Special services are available such as buying from the store, and multi-point deliveries!</p>
          </div>
        </div>
      </div>    {/*row */}

      {/* second row */}
      <div className="row">
        <div className="col-lg-4 col-md-4 col-sm-12 col-12">
          <div className="text d-flex">
          <i className="fa fa-magic icon"></i>
            <p>If you own a business you can opt for COD with us. Its cheap and effective. No reason to wait weeks for getting your revenue back.</p>
          </div>
          </div>
          <div className="col-lg-4 col-md-4 col-sm-12 col-12">
          <div className="text d-flex">
          <i className="fa fa-comment icon"></i>
            <p>If you own a business you can opt for COD with us. Its cheap and effective. No reason to wait weeks for getting your revenue back.</p>
          </div>
        </div>
        <div className="col-lg-4 col-md-4 col-sm-12 col-12">
          <div className="text d-flex">
          <i className="fa fa-id-card icon"></i>
            <p>We send courier's phone number to the contact person via SMS at each delivery point.</p>
          </div>
        </div>
      </div>    {/*row */}
   </div>
</div>     {/* LAYOUT SECTION */}
<hr></hr>   

{/* DELIVERY OPTIONS */}
{/* <div className="delivery-option">
   <h1>Available delivery options</h1>
    <div className="delivery-items">
       <div className="d-man"></div>
       <div className="d-bag"></div>
       <div className="d-bike"></div>
   </div>
   </div> */}

{/* Wefast is revolutionising  */}
   <div className="wefast-info">
      <h2>Wefast is revolutionising urgent deliveries </h2>
      <p>WeFast offers commercial and residential on-demand courier services all over Pune City. Our on-demand services are fast and reliable. Whether you require the delivery of documents, packages, online shopping, flowers, gifts, heavy goods of up to 200Kg or even emergency deliveries, WeFast can provide it all.<br /><br />

Most of our freelancers are students, artists, and designers looking to make extra money. We take great pride in providing them with real earning opportunities without the need to give up their studies or main jobs. In addition, we spend a lot of time and effort ensuring that all our freelancers are highly adaptable, have excellent communication skills, and are enthusiastic about working with us.<br /> <br />

Our services are responsive, confidential, friendly and affordable. We have been providing courier services in Pune for several years, which has enhanced our experience and given us a wealth of knowledge about the city.<br /> <br />

Throughout the years, we have set a strong track record with happy and satisfied customers. Our testimonials and reviews speak for themselves as we are ranked highly amongst all the courier service companies in Pune.<br /><br />

How Does Our Courier Service Work? Our express delivery system is designed to be as efficient as possible and is geared towards customer satisfaction.<br /><br />

You can book a trusted courier by simply completing our online order form with details such as phone numbers, addresses, time and weight of delivery, and so on. Choose between the more affordable walking couriers or faster motorcycle deliveries. The form takes about one to two minutes to complete, after which you get your quote and order number.<br /> <br />

After payment, the selected rider or courier will contact you to get more information about your parcel. If you are ordering our delivery services for a business, you can opt for cash on delivery (COD) which is more efficient.<br /><br />

The couriers collect your parcel within seven minutes and deliver it to your preferred destination in as little as 90 minutes. On delivery, the courier will require your signature on his smartphone screen to verify that all is in order.<br /><br />
Our operators are on call 24/7 in case of any questions or complaints you may have. Our system assigns the courier nearest to you with the highest ratings. It is therefore important to rate them so that we can assign the best couriers to other customers. </p>

<button type="submit" className="btn btn-success "><span>Book a courier</span></button>
   </div>

   <hr></hr>
   {/* footer section */}
   <div className="f-data">
     <div className="container">
       <div className="row">
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <img src={Logo} />
           <ul>
             <li><a href="#">Mumbai</a></li>
             <li><a href="#">Pune</a></li>
             <li><a href="#">Kolkata</a></li>
             <li><a href="#">Delhi/NCR</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* second column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">Subscription Plans</a></li>
             <li><a href="/FaQ">FAQ</a></li>
             <li><a href="#">Disclaimers & Dispute Resolution</a></li>
             <li><a href="#">Refund and Cancellation</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* third column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">About Us</a></li>
             <li><a href="/Privacy">Privacy and Policies</a></li>
             <li><a href="#">Contact</a></li>
             <li><a href="#">Track Order</a></li>
             <li><a href="#">Courier Service in Pune</a></li>
             <li><a href="#">pricing</a></li>
             <li><a href="/TermCondition">Terms and Conditions</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* fourth column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
           <div className="play-store">
             <li><a href="#"><div className="app-store"></div></a></li>
             <li><a href="#"><div className="play-store"></div></a></li>
             <li><a href="#"><div className="app-gallery"></div></a></li>
             </div>
           </ul>
         </div>     {/*col-lg-3*/}
       </div>
     </div>
   </div>    {/*f-data */}
{/* social-media section */}
 <div className="social-section">
  <div className="s-icon">
    <i className="fa fa-facebook-square icon"></i>
    <i className="fa fa-youtube-square icon"></i>
     <i className="fa fa-instagram icon"></i>
    <i className="fa fa-youtube-square icon"></i>
  </div>
   <p>© 2017–2021 Wefast. All Rights reserved.</p> 
 </div>      {/* social section */} 
   </>
 ) 
}; 

export default Home;