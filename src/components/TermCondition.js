import React from 'react';
import Logo from './images/logo.svg';



const Privacy=()=>{
  return(
      <>
        <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light">
   <div className="container">
  <a className="navbar-brand" href="#"><img src={Logo}/></a>
   <i className="fa fa-map-marker-alt fa-map-marker map-marker"></i>

  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item dropdown">
        <a className="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pune
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Action</a>
          <a className="dropdown-item" href="#">Another action</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Become a New Customer</a>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <button className="btn my-2 my-sm-0" type="submit">Order Delivery</button>
      <button className="btn my-2 my-sm-0" type="submit"><a href="signup.js"></a>Login/Signup</button>
    </form>
  </div>
  </div>
</nav>

    <br></br><br></br><br></br>
    {/* Privacy section */}

    <h1>Terms and conditions</h1>
    <br></br>
    <div className="container">
    <a style={{color: "Green", marginRight:"800px"}} href="/TermOfUse">Terms Of Use</a><br></br>
    <a style={{color: "Green", marginRight:"744px"}} href="/TermAndCondition">Terms and Conditions</a><br></br>
    <a style={{color: "Green", marginRight:"727px"}} href="/AndroidAppPermission">Android App Permission</a><br></br>
    <a style={{color: "Green", marginRight:"755px"}} href="/IosAppPermission">iOS App Permission</a><br></br>
    </div>

    <hr></hr>
   {/* footer section */}
   <div className="f-data">
     <div className="container">
       <div className="row">
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <img src={Logo} />
           <ul>
             <li><a href="#">Mumbai</a></li>
             <li><a href="#">Pune</a></li>
             <li><a href="#">Kolkata</a></li>
             <li><a href="#">Delhi/NCR</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* second column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">Subscription Plans</a></li>
             <li><a href="/FaQ">FAQ</a></li>
             <li><a href="#">Disclaimers & Dispute Resolution</a></li>
             <li><a href="#">Refund and Cancellation</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* third column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">About Us</a></li>
             <li><a href="/Privacy">Privacy and Policies</a></li>
             <li><a href="#">Contact</a></li>
             <li><a href="#">Track Order</a></li>
             <li><a href="#">Courier Service in Pune</a></li>
             <li><a href="#">pricing</a></li>
             <li><a href="/TermCondition">Terms and Conditions</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* fourth column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
           <div className="play-store">
             <li><a href="#"><div className="app-store"></div></a></li>
             <li><a href="#"><div className="play-store"></div></a></li>
             <li><a href="#"><div className="app-gallery"></div></a></li>
             </div>
           </ul>
         </div>     {/*col-lg-3*/}
       </div>
     </div>
   </div>    {/*f-data */}
{/* social-media section */}
 <div className="social-section">
  <div className="s-icon">
    <i className="fa fa-facebook-square icon"></i>
    <i className="fa fa-youtube-square icon"></i>
     <i className="fa fa-instagram icon"></i>
    <i className="fa fa-youtube-square icon"></i>
  </div>
   <p>© 2017–2021 Wefast. All Rights reserved.</p> 
 </div>      {/* social section */} 
      </>


  )
};

export default Privacy;