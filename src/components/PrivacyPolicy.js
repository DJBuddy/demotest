import React from 'react';
import Logo from './images/logo.svg';



const Privacy=()=>{
  return(
      <>
        <nav className="navbar navbar-expand-lg fixed-top navbar-light bg-light">
   <div className="container">
  <a className="navbar-brand" href="#"><img src={Logo}/></a>
   <i className="fa fa-map-marker-alt fa-map-marker map-marker"></i>

  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item dropdown">
        <a className="nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pune
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <a className="dropdown-item" href="#">Action</a>
          <a className="dropdown-item" href="#">Another action</a>
          <div className="dropdown-divider"></div>
          <a className="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li className="nav-item">
        <a className="nav-link" href="#">Become a New Customer</a>
      </li>
    </ul>
    <form className="form-inline my-2 my-lg-0">
      <button className="btn my-2 my-sm-0" type="submit">Order Delivery</button>
      <button className="btn my-2 my-sm-0" type="submit"><a href="signup.js"></a>Login/Signup</button>
    </form>
  </div>
  </div>
</nav>

    <br></br><br></br><br></br>
    {/* Privacy section */}


    <br></br>
    <div className="container">   <h2>d5-Privacy Policy</h2>
   Online Privacy Policy
What information do we collect?
We collect information from you when you respond to a survey.

Google, as a third party vendor, uses cookies to serve ads on your site. Google's use of the DART cookie enables it to serve ads to your users based on their visit to your sites and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy..

What do we use your information for?
Any of the information we collect from you may be used in one of the following ways:

To improve our website

(we continually strive to improve our website offerings based on the information and feedback we receive from you)

Do we use cookies?
Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information

We use cookies to compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.

Do we disclose any information to outside parties?
We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.

Third party links
Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.
<br></br>
California Online Privacy Protection Act Compliance
Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.
<br></br>
Childrens Online Privacy Protection Act Compliance
We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.
<br></br>
Online Privacy Policy Only<br></br>
This online privacy policy applies only to information collected through our website and not to information collected offline.
<br></br>
Terms and Conditions<br></br>
Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at terms and conditions.
<br></br>
Your Consent<br></br>
By using our site, you consent to our privacy policy.

Changes to our Privacy Policy
If we decide to change our privacy policy, we will post those changes on this page, and/or update the Privacy Policy modification date below.

This policy was last modified on 2013-12-16
<br></br>
Contacting Us<br></br>
If there are any questions regarding this privacy policy you may contact us using the information below.
<br></br>
generator.lorem-ipsum.info<br></br>

126 Electricov St.

Kiev, Kiev 04176

Ukraine

contact@lorem-ipsum.info
</div>

<hr></hr>
   {/* footer section */}
   <div className="f-data">
     <div className="container">
       <div className="row">
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <img src={Logo} />
           <ul>
             <li><a href="#">Mumbai</a></li>
             <li><a href="#">Pune</a></li>
             <li><a href="#">Kolkata</a></li>
             <li><a href="#">Delhi/NCR</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* second column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">Subscription Plans</a></li>
             <li><a href="/FaQ">FAQ</a></li>
             <li><a href="#">Disclaimers & Dispute Resolution</a></li>
             <li><a href="#">Refund and Cancellation</a></li>
             <li><a href="#">Bengaluru</a></li>
             <li><a href="#">Ahemadabad</a></li>
             <li><a href="#">Chennai</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* third column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
             <li><a href="#">About Us</a></li>
             <li><a href="/Privacy">Privacy and Policies</a></li>
             <li><a href="#">Contact</a></li>
             <li><a href="#">Track Order</a></li>
             <li><a href="#">Courier Service in Pune</a></li>
             <li><a href="#">pricing</a></li>
             <li><a href="/TermCondition">Terms and Conditions</a></li>
           </ul>
         </div>     {/*col-lg-3*/}

         {/* fourth column */}
         <div className="col-lg-3 col-md-3 col-sm-12 col-12">
           <ul>
           <div className="play-store">
             <li><a href="#"><div className="app-store"></div></a></li>
             <li><a href="#"><div className="play-store"></div></a></li>
             <li><a href="#"><div className="app-gallery"></div></a></li>
             </div>
           </ul>
         </div>     {/*col-lg-3*/}
       </div>
     </div>
   </div>    {/*f-data */}
{/* social-media section */}
 <div className="social-section">
  <div className="s-icon">
    <i className="fa fa-facebook-square icon"></i>
    <i className="fa fa-youtube-square icon"></i>
     <i className="fa fa-instagram icon"></i>
    <i className="fa fa-youtube-square icon"></i>
  </div>
   <p>© 2017–2021 Wefast. All Rights reserved.</p> 
 </div>      {/* social section */} 
      </>


  )
};

export default Privacy;