import './App.css';
import Home from './components/Home';
import Privacy from './components/Privacy';
import PrivacyPolicy from './components/PrivacyPolicy';
import PrivacyLink from './components/PrivacyLink';
import FaQ from './components/FaQ';
import TermCondition from './components/TermCondition';
import TermOfUse from './components/TermOfUse';
import TermAndCondition from './components/TermAndCondition';
import AndroidAppPermission from './components/AndroidAppPermission';
import IosAppPermission from './components/IosAppPermission';
// import { Route,Switch } from "react-router-dom";
// import {Switch, Route } from 'react-router-dom';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';

function App() {
  return (
    
    <div className="App">
      <Router>

        <Link to="">Home</Link>
        <Link to="/Privacy">Privacy</Link>
        <Link to="/PrivacyPolicy">PrivacyPolicy</Link>
        <Link to="/PrivacyPolicy">PrivacyLink</Link>
        <Link to="/FaQ">FaQ</Link>
        <Link to="/TermCondition">TermCondition</Link>
        <Link to="/TermOfUse">TermOfUse</Link>
        <Link to="/TermAndCondition">TermAndConditon</Link>
        <Link to="/AndroidAppPermission">AndroidAppPermission</Link>
        <Link to="/IosAppPermission">IosAppPermission</Link>
       <Route exact path="/" component={Home}/>
       <Route path="/Privacy" component={Privacy}/>
       <Route path="/PrivacyPolicy" component={PrivacyPolicy}/>
       <Route path="/PrivacyLink" component={PrivacyLink}/>
       <Route path="/FaQ" component={FaQ}/>
       <Route path="/TermCondition" component={TermCondition}/>
       <Route path="/TermOfUse" component={TermOfUse}/>
       <Route path="/TermAndCondition" component={TermAndCondition}/>
       <Route path="/AndroidAppPermission" component={AndroidAppPermission}/>
       <Route path="/IosAppPermission" component={IosAppPermission}/>

      </Router>
    </div>
 
  );
}

export default App;

